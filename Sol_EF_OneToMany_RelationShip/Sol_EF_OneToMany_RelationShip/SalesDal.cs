﻿using Sol_EF_OneToMany_RelationShip.EF;
using Sol_EF_OneToMany_RelationShip.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_OneToMany_RelationShip
{
    public class SalesDal
    {
        #region Declaration
        private AdventureWorks2012Entities db = null;
        #endregion

        #region Constructor
        public SalesDal()
        {
            db = new AdventureWorks2012Entities();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<SalesOrderHeaderEntity>> GetSalesData()
        {
            return await Task.Run(() => {

                var getQuery =
                    db
                    ?.SalesOrderHeaders
                    ?.AsEnumerable()
                    ?.Select(
                        
                        (leSalesOrderHeaderObj) => new SalesOrderHeaderEntity()
                        {
                            SalesOrderId = leSalesOrderHeaderObj.SalesOrderID,
                            SalesOrderNo = leSalesOrderHeaderObj.SalesOrderNumber,

                            //SalesOrderDetail = leSalesOrderHeaderObj
                            //                                .SalesOrderDetails
                            //                                .AsEnumerable()
                            //                                .Select((leSalesOrderDetailsObj) => new SalesOrderDetailsEntity()
                            //                                {
                            //                                    SalesOrderId = leSalesOrderDetailsObj.SalesOrderID,
                            //                                    OrderQty = leSalesOrderDetailsObj.OrderQty,
                            //                                    UnitPrice = leSalesOrderDetailsObj.UnitPrice
                            //                                })
                            //                                .ToList()

                            SalesOrderDetail= this.GetSalesOrderDetails(leSalesOrderHeaderObj).Result
                        }

                        )
                        ?.ToList();

                return getQuery;

            });
        }
        #endregion

        #region Private Method
        private async Task<List<SalesOrderDetailsEntity>> GetSalesOrderDetails(SalesOrderHeader salesOrderHeaderObj)
        {
            return await Task.Run(() => {

                var getQuery =
                        salesOrderHeaderObj
                        .SalesOrderDetails // Navigation Property
                        .AsEnumerable()
                        .AsEnumerable()
                        .Select((leSalesOrderDetailsObj) => new SalesOrderDetailsEntity()
                        {
                            SalesOrderId = leSalesOrderDetailsObj.SalesOrderID,
                            OrderQty = leSalesOrderDetailsObj.OrderQty,
                            UnitPrice = leSalesOrderDetailsObj.UnitPrice
                        })
                                                            .ToList();

                return getQuery;

            });
        }
        #endregion 
    }
}
