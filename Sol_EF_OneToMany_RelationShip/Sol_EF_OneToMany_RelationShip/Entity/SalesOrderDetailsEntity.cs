﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_OneToMany_RelationShip.Entity
{
    public class SalesOrderDetailsEntity
    {
        public int SalesOrderId { get; set; }

        public short OrderQty { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
