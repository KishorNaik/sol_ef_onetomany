﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_OneToMany_RelationShip.Entity
{
    public class SalesOrderHeaderEntity
    {
        public int SalesOrderId { get; set; }

        public String SalesOrderNo { get; set; }

        public List<SalesOrderDetailsEntity> SalesOrderDetail { get; set; }
    }
}
